#include <stdio.h>
#include <stdlib.h>

int main(void) {

    //Suma 2 numeros
    int primerValor, segundoValor;
    int sumaResultado;

    printf("\nIngrese un valor: ");
    scanf("\n %d", &primerValor);

    printf("\nIngrese otro valor: ");
    scanf("\n %d", &segundoValor);

    sumaResultado = primerValor + segundoValor;

    printf("\n La suma de %d + %d es igual a = %d", primerValor, segundoValor, sumaResultado);

}